package com.example.whatsappmvvm.data.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Chat(
    var avatar: Int = 0,
    var name: String? = "",
    var message: String? = "",
    var time: String? =""
) : Parcelable
