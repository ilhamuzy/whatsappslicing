package com.example.whatsappmvvm.data

import com.example.whatsappmvvm.R
import com.example.whatsappmvvm.data.model.Chat

object ChatData {
    private val image: Array<Int> =
        arrayOf(
            R.drawable.user1,
            R.drawable.user2,
            R.drawable.user3,
            R.drawable.user4,
            R.drawable.user5,
            R.drawable.user6,
            R.drawable.user7,
            R.drawable.user8,
            R.drawable.user9,
            R.drawable.user10
        )
    private val name: Array<String> =
        arrayOf(
            "Bill Gates",
            "Steve Jobs",
            "Rocky Balboa",
            "Eminem",
            "Younglex",
            "Adam Levine",
            "Jeff Bazos",
            "Arsene Wenger",
            "Mikel Arteta",
            "Jackie Chan"
        )

    private val message: Array<String> =
        arrayOf(
            "How are you today?",
            "Okay bro",
            "Thanks",
            "?",
            "Thanks bro",
            "Let join us bro",
            "I do apologize bro..",
            "Thanks",
            "Hey bro",
            "Thanks bro"
        )
    private val time: Array<String> =
        arrayOf(
            "16.00",
            "15.45",
            "Yesterday",
            "Yesterday",
            "03/05/21",
            "03/05/21",
            "02/05/21",
            "02/05/21",
            "01/05/21",
            "01/05/21",
        )

    val listData: ArrayList<Chat>
    get(){
        val list : ArrayList<Chat> = arrayListOf<Chat>()
        for (i in image.indices){
            val chat = Chat()
            chat.avatar = image[i]
            chat.name = name[i]
            chat.message = message[i]
            chat.time = time[i]
            list.add(chat)
        }
        return list
    }
}