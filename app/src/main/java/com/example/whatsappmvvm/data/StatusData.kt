package com.example.whatsappmvvm.data

import com.example.whatsappmvvm.R
import com.example.whatsappmvvm.data.model.Status

object StatusData {
    private val image: Array<Int> =
        arrayOf(
            R.drawable.user1,
            R.drawable.user2,
            R.drawable.user3,
            R.drawable.user4,
            R.drawable.user5,
            R.drawable.user6,
            R.drawable.user7,
            R.drawable.user8,
            R.drawable.user9,
            R.drawable.user10
        )
    private val name: Array<String> =
        arrayOf(
            "Bill Gates",
            "Steve Jobs",
            "Rocky Balboa",
            "Eminem",
            "Younglex",
            "Adam Levine",
            "Jeff Bazos",
            "Arsene Wenger",
            "Mikel Arteta",
            "Jackie Chan"
        )
    private val time: Array<String> =
        arrayOf(
            "Today 16.00",
            "Today 15.45",
            "Yesterday 18.00",
            "Yesterday 18.00",
            "Yesterday 18.00",
            "Yesterday 18.00",
            "Yesterday 18.00",
            "Yesterday 18.00",
            "Yesterday 18.00",
            "Yesterday 18.00"
        )
    val listData: ArrayList<Status>
    get() {
        val list : ArrayList<Status> = arrayListOf<Status>()
        for (i in image.indices){
            val status = Status()
            status.avatar = image[i]
            status.name = name[i]
            status.time = time[i]
            list.add(status)
        }
        return list
    }
}