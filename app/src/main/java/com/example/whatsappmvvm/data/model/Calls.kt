package com.example.whatsappmvvm.data.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Calls(
    var avatarCall: Int = 0,
    var nameCall: String? = "",
    var timeCall: String? = "",
    var callType: Int? = 0,
    var statusCall: Int? = 0
) : Parcelable
