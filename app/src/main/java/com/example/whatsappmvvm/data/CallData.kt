package com.example.whatsappmvvm.data

import com.example.whatsappmvvm.R
import com.example.whatsappmvvm.data.model.Calls

object CallData {
    private val imageCall: Array<Int> =
        arrayOf(
            R.drawable.user1,
            R.drawable.user2,
            R.drawable.user3,
            R.drawable.user4,
            R.drawable.user5,
            R.drawable.user6,
            R.drawable.user7,
            R.drawable.user8,
            R.drawable.user9,
            R.drawable.user10
        )
    private val nameCall: Array<String> =
        arrayOf(
            "Bill Gates",
            "Steve Jobs",
            "Rocky Balboa",
            "Eminem",
            "Younglex",
            "Adam Levine",
            "Jeff Bazos",
            "Arsene Wenger",
            "Mikel Arteta",
            "Jackie Chan"
        )
    private val timeCall: Array<String> =
        arrayOf(
            "Hari ini 15.30",
            "Hari ini 15.20",
            "Hari ini 15.10",
            "Hari ini 15.00",
            "Hari ini 12.00",
            "Kemarin 13.20",
            "Kemarin 11.20",
            "Kemarin 10.00",
            "Kemarin 07.00",
            "22 April 20.42"
        )
    private val callType: Array<Int> =
        arrayOf(
            R.drawable.ic_call,
            R.drawable.ic_call,
            R.drawable.ic_videocall,
            R.drawable.ic_videocall,
            R.drawable.ic_call,
            R.drawable.ic_videocall,
            R.drawable.ic_call,
            R.drawable.ic_call,
            R.drawable.ic_videocall,
            R.drawable.ic_videocall
        )

    private val statusCall: Array<Int> =
        arrayOf(
            R.drawable.ic_incoming_call,
            R.drawable.ic_incoming_call,
            R.drawable.ic_outgoing_call,
            R.drawable.ic_missing_call,
            R.drawable.ic_incoming_call,
            R.drawable.ic_missing_call,
            R.drawable.ic_outgoing_call,
            R.drawable.ic_outgoing_call,
            R.drawable.ic_incoming_call,
            R.drawable.ic_missing_call
        )


    val listData: ArrayList<Calls>
        get() {
            val list : ArrayList<Calls> = arrayListOf<Calls>()
            for (i in imageCall.indices){
                val call = Calls()
                call.avatarCall = imageCall[i]
                call.nameCall = nameCall[i]
                call.timeCall = timeCall[i]
                call.callType = callType[i]
                call.statusCall = statusCall[i]
                list.add(call)
            }
            return list
        }
}