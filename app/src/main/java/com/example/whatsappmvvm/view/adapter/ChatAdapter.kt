package com.example.whatsappmvvm.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.whatsappmvvm.R
import com.example.whatsappmvvm.data.model.Chat
import kotlinx.android.synthetic.main.chat_list_item.view.*

class ChatAdapter(var data: List<Chat>) : RecyclerView.Adapter<ChatAdapter.ViewHolder>() {
    inner class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView) {
        val img = itemView.avatar
        val nama = itemView.nameChat
        val message = itemView.message
        val time = itemView.time
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view : View = LayoutInflater.from(parent.context).inflate(R.layout.chat_list_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.nama.text = data?.get(position)?.name
        holder.message.text = data?.get(position)?.message
        holder.time.text = data?.get(position)?.time

        Glide.with(holder.itemView.context).load(data?.get(position)?.avatar).into(holder.img)
    }

    override fun getItemCount(): Int = data?.size ?: 0
}