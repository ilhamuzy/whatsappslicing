package com.example.whatsappmvvm.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.whatsappmvvm.R
import com.example.whatsappmvvm.view.adapter.CallAdapter
import com.example.whatsappmvvm.data.model.Calls
import com.example.whatsappmvvm.view.main.viewmodel.calls.CallBindView
import com.example.whatsappmvvm.view.main.viewmodel.calls.CallGetData
import kotlinx.android.synthetic.main.fragment_call.*

class CallsFragment : Fragment(), CallBindView {
    private var presenter : CallGetData? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_call, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        presenter = CallGetData(this)
        presenter?.getCallData()
    }

    override fun onSuccessBind(msg: String, chat: List<Calls?>?) {
        rv_call.apply{
            adapter = CallAdapter(chat as List<Calls>)
            setHasFixedSize(true)
        }
    }
}