package com.example.whatsappmvvm.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.whatsappmvvm.R
import com.example.whatsappmvvm.data.model.Calls
import kotlinx.android.synthetic.main.call_list_item.view.*

class CallAdapter  (var data: List<Calls>) :RecyclerView.Adapter<CallAdapter.ViewHolder>(){
    inner class ViewHolder(itemView:View) :RecyclerView.ViewHolder(itemView){
        val img = itemView.avatarCall
        val nama = itemView.nameCall
        val time = itemView.timeCall
        val statusCall = itemView.callStatus
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view : View = LayoutInflater.from(parent.context).inflate(R.layout.call_list_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.nama.text = data?.get(position)?.nameCall
        holder.time.text = data?.get(position)?.timeCall

        Glide.with(holder.itemView.context).load(data?.get(position)?.avatarCall).into(holder.img)
        Glide.with(holder.itemView.context).load(data?.get(position)?.statusCall).into(holder.statusCall)
    }

    override fun getItemCount(): Int = data?.size
}