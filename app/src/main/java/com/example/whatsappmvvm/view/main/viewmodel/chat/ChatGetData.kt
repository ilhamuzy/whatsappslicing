package com.example.whatsappmvvm.view.main.viewmodel.chat

import com.example.whatsappmvvm.data.ChatData

class ChatGetData (val bindView: ChatBindView){
    fun getChatData(){
        val dataChat = ChatData

        bindView.onSuccessBind("Success Bind Data",dataChat.listData)
    }
}