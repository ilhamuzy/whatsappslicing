package com.example.whatsappmvvm.view.main.viewmodel.chat

import com.example.whatsappmvvm.data.model.Chat

interface ChatBindView {
    fun onSuccessBind(msg: String, chat: List<Chat?>?)
}