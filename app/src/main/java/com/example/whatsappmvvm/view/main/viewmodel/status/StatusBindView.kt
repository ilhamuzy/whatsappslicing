package com.example.whatsappmvvm.view.main.viewmodel.status

import com.example.whatsappmvvm.data.model.Status

interface StatusBindView {

    fun onSuccess(msg:String, status: List<Status?>?)
}