package com.example.whatsappmvvm.view.main.viewmodel.calls

import com.example.whatsappmvvm.data.model.Calls

interface CallBindView {
    fun onSuccessBind(msg : String, chat: List<Calls?>?)
}