package com.example.whatsappmvvm.view.main

import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import android.view.Menu
import android.view.MenuItem
import com.example.whatsappmvvm.R
import com.example.whatsappmvvm.databinding.ActivityMainBinding
import com.example.whatsappmvvm.view.adapter.FragmentAdapterPage
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewpager_main.adapter = FragmentAdapterPage(supportFragmentManager)
        tabs_main.setupWithViewPager(viewpager_main)
    }
}