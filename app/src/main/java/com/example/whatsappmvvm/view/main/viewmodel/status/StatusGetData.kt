package com.example.whatsappmvvm.view.main.viewmodel.status

import com.example.whatsappmvvm.data.StatusData


class StatusGetData (val bindView: StatusBindView) {
    fun getDataStatus(){
        val dataStatus = StatusData
        bindView.onSuccess("Success Bind Status Data", dataStatus.listData)
    }
}