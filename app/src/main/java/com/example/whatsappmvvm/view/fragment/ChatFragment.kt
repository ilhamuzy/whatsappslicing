package com.example.whatsappmvvm.view.fragment

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import com.example.whatsappmvvm.R
import com.example.whatsappmvvm.view.adapter.ChatAdapter
import com.example.whatsappmvvm.view.main.viewmodel.chat.ChatBindView
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_chat.*

class ChatFragment() : Fragment(), ChatBindView {
    private lateinit var viewModel : ChatAdapter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_chat, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = ChatAdapter(this)
        viewModel?.getCall()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater!!.inflate(R.menu.main_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item!!.itemId

        if (id == R.id.new_grup) {
            Snackbar.make(chat_Container, "New Group", Snackbar.LENGTH_LONG).show()
        }
        return super.onOptionsItemSelected(item)
    }


    override fun onSuccessBind(msg: String, chat: List<Chat?>?) {
        rv_chat.apply {
            adapter = ChatAdapter(chat as List<Chat>)
            setHasFixedSize(true)
        }
    }
}