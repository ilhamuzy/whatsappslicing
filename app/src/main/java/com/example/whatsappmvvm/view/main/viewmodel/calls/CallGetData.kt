package com.example.whatsappmvvm.view.main.viewmodel.calls

import androidx.lifecycle.ViewModel
import com.example.whatsappmvvm.data.CallData

class CallGetData(val bindView: CallBindView) : ViewModel() {
    fun getCallData(){
        val dataCall = CallData

        bindView.onSuccessBind("Success Bind Data", dataCall.listData)
    }
}